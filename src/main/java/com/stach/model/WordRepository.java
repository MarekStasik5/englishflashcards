package com.stach.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class WordRepository {
	private List<Word>words;
	
	@Autowired
	public WordRepository(FileOperation fileOperations) {
		try {
			this.words=fileOperations.readFile();
		}catch(IOException e) {
			words=new ArrayList<Word>();
		}
	}
	
	public List<Word>getAll(){
		return words;
	}
	
	public Set<Word>getRandomWords(int number){
		Random random = new Random();
		Set<Word>randomWord=new HashSet<Word>();
		while(randomWord.size()<number && randomWord.size()<words.size()) {
			randomWord.add(words.get(random.nextInt(words.size())));
		}
		return randomWord;
	}

	public void add(Word word) {
		words.add(word);
	}
	
	public int size() {
		return words.size();
	}
	
	public boolean isEmpty() {
		return words.isEmpty();
	}
}
