package com.stach.model;


import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.stach.protection.CipherService;

@Service
public class FileOperation {
	
	private String fileName;
	private CipherService cipherService;
	
	@Autowired
	public FileOperation(@Value("${file.name}") String fileName,CipherService cipherService) {
		this.fileName = fileName;
		this.cipherService=cipherService;
	}

	List<Word>readFile() throws IOException{
		return Files.readAllLines(Paths.get(fileName))
				.stream()
				.map(cipherService::decrypt)
				.map(CsvWordConventer::parse)
				.collect(Collectors.toList());
	}

	public void saveWords(List<Word>words) throws IOException {
		BufferedWriter writer=new BufferedWriter(new FileWriter(fileName));
		for(Word word:words) {
			writer.write(cipherService.encrypt(word.toString()));
			writer.newLine();
		}
		writer.close();
	}
	
	private static class CsvWordConventer{
		static Word parse(String text) {
			String[] split=text.split(",");
			return new Word(split[0], split[1]);
		}
	}
}
