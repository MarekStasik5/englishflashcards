package com.stach.controller;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.stach.model.FileOperation;
import com.stach.model.Word;
import com.stach.model.WordRepository;
import com.stach.view.OutputConsoleWritter;

@Controller
public class WordController {
	
	private static final int UNDEFINED = -1;
    private static final int ADD_ENTRY = 0;
    private static final int TEST = 1;
    private static final int CLOSE_APP = 2;
    
    private WordRepository wordRepository;
    private FileOperation fileOperations;
    private Scanner scanner;
    private OutputConsoleWritter outputWritter;
    
    @Autowired
    public WordController(WordRepository wordRepository, FileOperation fileOperations, Scanner scanner, 
    		OutputConsoleWritter outputWritter) {
		this.wordRepository = wordRepository;
		this.fileOperations = fileOperations;
		this.scanner = scanner;
		this.outputWritter=outputWritter;
	}

	public void mainLop() {
    	outputWritter.print("Welcoome to fishcard aplication");
    	int option = UNDEFINED;
        while(option != CLOSE_APP) {
            printMenu();
            option = chooseOption();
            executeOption(option);
        }
    }
    
    private void executeOption(int option) {
        switch (option) {
            case ADD_ENTRY:
                addWord();
                break;
            case TEST:
                test();
                break;
            case CLOSE_APP:
                close();
                break;
            default:
                outputWritter.print("Somethink was wrong! Try again");
        }
    }
    
    private void test() {
    	if(wordRepository.isEmpty()) {
    		outputWritter.print("Add some word int database");
    		return;
    	}
        final int testSize = wordRepository.size() > 10? 10 : wordRepository.size();
    	Set<Word>randomWords=wordRepository.getRandomWords(testSize);
    	int score=0;
    	for(Word word:randomWords) {
    		System.out.printf("Translate: \"%s\"\n",word.getOriginal());
    		String translation=scanner.nextLine();
    		if(word.getTranslation().equalsIgnoreCase(translation)) {
    			outputWritter.print("Correct");
    			score++;
    		}else {
    		outputWritter.print("Wrong answer - "+ word.getTranslation());
    		}
    	}
    	System.out.printf("Your result: %d/%d\n", score, testSize);
    }
    
    private void addWord() {
    	outputWritter.print("Set new word");
    	String original=scanner.nextLine();
    	outputWritter.print("Set transaltion");
    	String translation = scanner.nextLine();
    	Word word = new Word(original, translation);
    	wordRepository.add(word);
    }
    
    private void close() {
    	try {
    		fileOperations.saveWords(wordRepository.getAll());
    		outputWritter.print("Word saved");
    	}catch(IOException e) {
    		outputWritter.print("Save error");
    	}
    }
    
    private void printMenu() {
    	outputWritter.print("Choose the activity:");
    	outputWritter.print("0 - Add word");
    	outputWritter.print("1 - Test");
    	outputWritter.print("2 - End of the program");
    }
    
    private int chooseOption() {
        int option;
        try {
            option = scanner.nextInt();
        } catch(InputMismatchException e) {
            option = UNDEFINED;
        } finally {
            scanner.nextLine();
        }
        if(option > UNDEFINED && option <= CLOSE_APP)
            return option;
        else
            return UNDEFINED;
    }
    
}
