package com.stach.view;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.stach.formater.TextFormater;

@Component
public class OutputConsoleWritter {

	private TextFormater textFormater;
	

	@Autowired
	public void setTextFormater(@Qualifier("upperCase") TextFormater textFormater) {
		this.textFormater = textFormater;
	}

	
	public void print(String input) {
		String outputText = textFormater.formater(input);
		System.out.println(outputText);
	}
}
