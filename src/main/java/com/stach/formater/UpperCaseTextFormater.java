package com.stach.formater;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("upperCase")
public class UpperCaseTextFormater implements TextFormater {

	@Override
	public String formater(String input) {
		return input.toUpperCase();
	}

}
