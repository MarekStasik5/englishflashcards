package com.stach.protection;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("prod")
public class CeasarCipherService implements CipherService {
	
	private static final int SHIFT=3;

	@Override
	public String encrypt(String input) {
		return input.chars().map(CeasarCipherService::shift)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
	}

	@Override
	public String decrypt(String input) {
		return input.chars().map(CeasarCipherService::shiftBack)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
	}

	private static int shift(int character) {
		return character+SHIFT;
	}
	
	private static int shiftBack(int character) {
		return character-SHIFT;
	}
	
}
