package com.stach.protection;

public interface CipherService {
	String encrypt(String input);
	String decrypt(String input);

}
