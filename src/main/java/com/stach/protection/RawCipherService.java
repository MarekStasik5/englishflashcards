package com.stach.protection;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("dev")
public class RawCipherService implements CipherService {

	@Override
	public String encrypt(String input) {
		return input;
	}

	@Override
	public String decrypt(String input) {
		return input;
	}

}
