package com.stach;

import java.util.Scanner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;

import com.stach.controller.WordController;


@SpringBootApplication
public class EnglishStudyApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext ctx=SpringApplication.run(EnglishStudyApplication.class, args);
		WordController controller=ctx.getBean(WordController.class);
		controller.mainLop();
	}
	
	@Bean
	Scanner scanner() {
		return new Scanner(System.in);
	}
}
